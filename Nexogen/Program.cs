﻿using Flurl.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Nexogen
{
    class Program
    {
        private static readonly string fileUrl = "https://nexogenshares.blob.core.windows.net/recruitment/mediordev.txt";
        static void Main(string[] args)
        {
            string downloadedFilePAth = "mediordev.txt";
            string[][] trucks = null;
            if (!File.Exists(downloadedFilePAth))
            {
                Task.WaitAny(fileUrl.WithTimeout(10).DownloadFileAsync(".", downloadedFilePAth));
            }
            string line;
            var truckCount = 0;
            int truckIndex = 0;
            using var fileStream = File.OpenRead(downloadedFilePAth);
            using StreamReader reader = new(fileStream);
            using var streamWriter = File.CreateText("result.txt");
            while ((line = reader.ReadLine()) != null)
            {
                var orderInfo = line.Split(' ');
                if (orderInfo.Length == 1 && trucks is null &&  int.TryParse(orderInfo[0], out truckCount))
                {
                    trucks = new string[truckCount][];
                    continue;
                }

                
                if (truckCount > truckIndex)
                {
                    trucks[truckIndex] = orderInfo;
                    truckIndex++;
                }
                else
                {
                    if (orderInfo.Length == 1) continue;
                    for (int i = 0; i < trucks.Length; i++)
                    {
                        if (trucks[i].Skip(1).Intersect(orderInfo.Skip(1)).Any())
                        {
                            streamWriter.WriteLine($"{trucks[i][0]} {orderInfo[0]}");
                            trucks[i] = Array.Empty<string>();
                            break;
                        }
                        
                    }
                }
            }           
        }
    }
}
